## Introduction

This repository is a base template for programming firmwares using platformio.
It uses gitlab-CI and docker to compile the binary and upload to gitlab.
I like this approach since I know everything is self contained.
You will however still need a programmer software to upload the binary to your
target. This can be done with tools such as `esptool` or `avrdude` . I still
believe that this approach is still simpler in spite of this.

### Basic steps to get up and going quick

1) Copy repo into new project, Set up runners as necessary and change 
name and settings as desired.

2) Update platformio.ini to match the board type you are using.
Note that multiple boards can be defined and all of them will be compiled.

3) Update .gitlab-ci.yml to upload the binary. Path follows pattern of
`.pioenvs/[board]/firmware.bin` so just replace the [board] part to
match the board as defined in step 2. Note that if multiple boards are
defined in (2) then multiple upload lines will need to be added here.

4) Edit your ino/cpp file in /src and upload any pertinent libraries to /lib.
Also add any lib depencies for libraries found searching at 
[platformio_Search](https://platformio.org/lib/search) to the appropriate
env found in the base platformio.ini file (something like 
`lib_deps = MQTTClient`).

5) On commit, it will compile and you can 
download bin file from the artifacts directory. Note that default is to
store for only one week. Change this for persistent binaries.

The compiled source is now done. Last step is to get the bin onto the board.
USB and UDEV setups are not covered here for now. 

### Upload design

for ESP devices use esptool (or esptool.py) with something like,
```sh
esptool.py -p /dev/ttyUSB0 write_flash -fm dio 0x0 firmware.bin
```

Or if that doesn't work try adding the flash size with the following command

```sh
esptool.py --port /dev/ttyUSB0 write_flash --flash_mode dio --flash_size 4MB 0x0 firmware.bin
```

If programming tasmotta firmwares use the following
```sh
esptool.py -p /dev/ttyUSB0 write_flash -fs 1MB -fm dout 0x0 firmware.bin
```

I'm not clear on the offsetting and which version require bootloader, etc.
This is very much a WIP and will need updating.



